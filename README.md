# CIS

This playbook runs a CIS benchmarking script against the specified system and produces a CSV report.

## Pre-requisites

## Usage
```
git clone https://gitlab.com/octo.tech/cis.git
cd cis
ansible-galaxy install --roles-path roles/ -r roles/requirements.yml
ansible-playbook -i <hostname>, playbook.yml
```

We ignore the roles directories to keep playbook repo tidy.  To pull updates to all roles incant
```
ansible-galaxy install --roles-path roles/ -r roles/requirements.yml --force
```

## Bugs
If you have any problems using this role please raise an [Issue](https://gitlab.com/octo.tech/cis/-/issues).

